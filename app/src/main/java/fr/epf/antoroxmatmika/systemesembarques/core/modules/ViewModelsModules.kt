package fr.epf.antoroxmatmika.systemesembarques.core.modules

import org.koin.dsl.module.module
import org.koin.androidx.viewmodel.ext.koin.viewModel
import fr.epf.antoroxmatmika.systemesembarques.ui.MainViewModel


val viewModelsModule = module {
    viewModel {
        MainViewModel(adafruitDataSource = get())
    }
}