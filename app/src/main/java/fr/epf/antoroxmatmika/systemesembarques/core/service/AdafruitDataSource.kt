package fr.epf.antoroxmatmika.systemesembarques.core.service

import fr.epf.antoroxmatmika.systemesembarques.model.FeedDataResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AdafruitDataSource(private val adafruitApiService: AdafruitApiService) {
    fun getFeedData(username: String, aioKey: String,feedKey: String,handleSuccess :(List<FeedDataResponse>?) -> Unit,handleFail: (Throwable) -> Unit) {
        adafruitApiService.getFeedDataByKey(aioKey, username, feedKey)
            .enqueue(handleResponse(handleSuccess, handleFail))
    }
}

private fun <T> handleResponse(
    handleSuccess: (T?) -> Unit,
    handleFail: (Throwable) -> Unit
): Callback<T> {
    return object : Callback<T> {
        override fun onFailure(call: Call<T>, t: Throwable) {
            handleFail(t)
        }

        override fun onResponse(call: Call<T>, response: Response<T>) {
            handleSuccess(response.body())
        }
    }
}