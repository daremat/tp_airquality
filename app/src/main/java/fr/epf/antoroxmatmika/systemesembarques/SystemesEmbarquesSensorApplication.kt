package fr.epf.antoroxmatmika.systemesembarques

import android.app.Application
import fr.epf.antoroxmatmika.systemesembarques.core.modules.coreModule
import fr.epf.antoroxmatmika.systemesembarques.core.modules.viewModelsModule
import timber.log.Timber
import org.koin.android.ext.android.startKoin

class SystemesEmbarquesSensorApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())

        startKoin(
            this@SystemesEmbarquesSensorApplication
            , modules = listOf(coreModule, viewModelsModule)
        )

    }
}