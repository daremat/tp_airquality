package fr.epf.antoroxmatmika.systemesembarques.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import fr.epf.antoroxmatmika.systemesembarques.R
import org.koin.androidx.viewmodel.ext.android.viewModel
import android.graphics.Color
import android.graphics.DashPathEffect
import androidx.lifecycle.Observer
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.IFillFormatter
import fr.epf.antoroxmatmika.systemesembarques.model.FeedDataPoint
import fr.epf.antoroxmatmika.systemesembarques.ui.formatter.TimeFormatter
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {

    private val mainViewModel: MainViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initChart()

        val feedKeyObserver = Observer<String> { mainViewModel.getFeedData() }
        val feedDataObserver = Observer<List<FeedDataPoint>> {
            chart.description.text = mainViewModel.feedKey.value

            val leftAxis = chart.axisLeft
            // set min interval for the axis when zooming
            leftAxis.granularity = 0.25f
            leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART)

            val xAxis = chart.xAxis
            xAxis.position = XAxis.XAxisPosition.BOTTOM
            // format value to given format
            xAxis.valueFormatter = TimeFormatter()
            // limit value shown on xAxis
            xAxis.labelCount = 6

            val values = ArrayList<Entry>()
            it.reversed().forEach { item ->
                values.add(Entry(item.created_at, item.value))
            }

            val lineDataSet = LineDataSet(values, "DataSet 1")

            // format curve format and values appearance
            lineDataSet.disableDashedLine()
            lineDataSet.setDrawCircles(false)
            lineDataSet.setDrawValues(false)

            // create a data object with the data sets
            val data = LineData(lineDataSet)
            data.setValueTextSize(20f)

            // black lines and points
            lineDataSet.color = Color.BLACK
            lineDataSet.setCircleColor(Color.BLACK)

            // line thickness and point size
            lineDataSet.lineWidth = 1f
            lineDataSet.circleRadius = 3f

            // draw points as solid circles
            lineDataSet.setDrawCircleHole(false)

            // customize legend entry
            lineDataSet.formLineWidth = 1f
            lineDataSet.formLineDashEffect = DashPathEffect(floatArrayOf(10f, 5f), 0f)
            lineDataSet.formSize = 15f

            // text size of values
            lineDataSet.valueTextSize = 9f

            // draw selection line as dashed
            lineDataSet.enableDashedHighlightLine(10f, 5f, 0f)

            // set the filled area
            lineDataSet.setDrawFilled(true)
            lineDataSet.fillFormatter =  IFillFormatter { _, _ -> chart.axisLeft.axisMinimum }

            // set data
            chart.data = data
            chart.invalidate()
        }
        mainViewModel.feedData.observe(this, feedDataObserver)
        mainViewModel.feedKey.observe(this, feedKeyObserver)
    }

    private fun initChart() {
        // animate
        chart.animateX(3000)
        chart.axisRight.isEnabled = false
        chart.legend.isEnabled = false
        chart.description.isEnabled = false

        // background color
        chart.setBackgroundColor(Color.WHITE)

        // disable description text
        chart.description.isEnabled = false

        // enable touch gestures
        chart.setTouchEnabled(true)

        // set listeners
        chart.setDrawGridBackground(false)

        // enable scaling and dragging
        chart.isDragEnabled = false
        chart.setScaleEnabled(false)

        // force pinch zoom along both axis
        chart.setPinchZoom(false)
    }
}