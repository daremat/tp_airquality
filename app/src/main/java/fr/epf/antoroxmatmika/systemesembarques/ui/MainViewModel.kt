package fr.epf.antoroxmatmika.systemesembarques.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import fr.epf.antoroxmatmika.systemesembarques.core.service.AdafruitDataSource
import fr.epf.antoroxmatmika.systemesembarques.model.FeedDataPoint


class MainViewModel(val adafruitDataSource: AdafruitDataSource) : ViewModel() {

    private val _feedData = MutableLiveData<List<FeedDataPoint>>()
    val feedData: LiveData<List<FeedDataPoint>> = _feedData

    private val _feedKey = MutableLiveData<String>()
    val feedKey: LiveData<String> = _feedKey

    fun getFeedData() {
        val feedKey = "AirQuality"
        val user = "mdarras62"

        adafruitDataSource.getFeedData(
            user, "3ba5e5152e104f29b6704596126041e6",  feedKey,
            { 	data ->
                val points = mutableListOf<FeedDataPoint>()
                data?.forEach {
                    points.add(FeedDataPoint(value = it.value.toFloat(),  created_at = it.created_at.time.toFloat()))
                }
                //_feedData.postValue(points)
            }, {
                //_feedData.postValue(emptyList())
            })
    }
}