package fr.epf.antoroxmatmika.systemesembarques.core.service

import fr.epf.antoroxmatmika.systemesembarques.model.FeedDataResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path
import retrofit2.http.Query

interface AdafruitApiService {

    @GET("/api/v2/{username}/feeds/{feed_key}/data")
    fun getFeedDataByKey(
        @Header("X-AIO-Key") aioKey: String,
        @Path("username") username: String, @Path("feed_key") feedKey: String, @Query("start_time") limit: String = "2019-09-02T14:25"
    ): Call<List<FeedDataResponse>>
}